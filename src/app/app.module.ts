import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule, MatInputModule } from '@angular/material';

import { AppComponent } from './app.component';
import { HeaderComponent } from './app-header/app-header.component';
import { TableModule } from './table/table.module';
import { MoreThan25Pipe } from './shared/pipes/moreThan25.pipe';
import { appRoutes } from './router/router.const';
import { ReactFormComponent } from './react-form/react-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ReactFormComponent,
    MoreThan25Pipe,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    TableModule,
    MatButtonModule,
    MatInputModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
