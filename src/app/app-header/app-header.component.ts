import { Component, OnDestroy } from '@angular/core';
import { IUser } from '../shared/models/interfaces/app.models';
import { ProfileService } from '../shared/services/profile.service';
import { Subscription } from 'rxjs';
import { usersConst } from '../shared/models/const/app.const';


@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class HeaderComponent implements OnDestroy {
  user: IUser = usersConst[0];
  userImageUrl: string = this.user.userImage;
  subscription: Subscription;

  constructor(private profileService: ProfileService) {
    this.subscription = this.profileService.getCurrentUser().subscribe(selectedUser => { this.user = selectedUser; });
  }

  ngOnDestroy(): void {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
}


