import { Component } from '@angular/core';
import { usersConst } from './shared/models/const/app.const';
import { IUser } from './shared/models/interfaces/app.models';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  users: IUser[] = usersConst;
  selectedUser: IUser = usersConst[0];
  isModalOpened = false;

  openModal(id: string): void {
    this.isModalOpened = true;
  }

  closeModal(): void {
    this.isModalOpened = false;
  }

}
