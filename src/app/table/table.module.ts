import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { HighlightDirective } from '../shared/directives/highlight.directive';
import { SearchFilterPipe } from '../shared/pipes/search-filter.pipe';
import { MatInputModule, MatTableModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatTableModule
  ],
  declarations: [TableComponent, HighlightDirective, SearchFilterPipe],
  exports: [TableComponent]
})
export class TableModule { }
