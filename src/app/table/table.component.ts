import { Component, Output, EventEmitter } from '@angular/core';
import { IUser } from '../shared/models/interfaces/app.models';
import { usersConst } from '../shared/models/const/app.const';
import { ProfileService } from '../shared/services/profile.service';
import { Subscription } from 'rxjs';
import { UsersService } from '../shared/services/users.service';
import { fadeAnimation } from '../shared/animations/fade.animation';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-table',
  animations: [fadeAnimation],
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {

  users: IUser[];
  usersDataSource: MatTableDataSource<any>;
  subscription: Subscription;
  displayedColumns: string[] = ['userName', 'userAge', 'userOccupation'];
  filterString = '';


  constructor(private profileService: ProfileService, private usersService: UsersService, private router: Router) {
    this.subscription = this.usersService.getUsers().subscribe(
      (users: IUser[]) => { this.usersDataSource = new MatTableDataSource(users); },
      err => console.log(err));
  }

  onSelect(user: IUser): void {
    // send message to subscribers via observable subject
    this.profileService.setCurrentUser(user);
  }

  onEdit(userIndex) {
    this.router.navigate([`/feature/${userIndex}`]);
  }

  onDelete(userIndex) {
    this.usersService.deleteUser(userIndex);
  }

  applyFilter(value: string) {
    this.usersDataSource.filterPredicate = (data, filterValue: string) => {
      if (data.userName.toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1 ||
        data.userAge.indexOf(filterValue) > -1 ||
        data.userOccupation.toLowerCase().indexOf(filterValue.trim().toLowerCase()) > -1) {
        return true;
      }
    };
    this.usersDataSource.filter = value.trim().toLowerCase();

  }
  // filterCheck(event): void {
  //   this.filterString = event.target.value;
  // }

}
