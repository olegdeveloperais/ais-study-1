export interface IUser {
  userName: string;
  userImage: string;
  userAge: string;
  userOccupation: string;
}
