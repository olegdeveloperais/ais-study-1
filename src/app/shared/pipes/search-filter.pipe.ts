import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from '../models/interfaces/app.models';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(users: IUser[], args?: any): any {
    if (!args) { return users; }
    return users.filter(user => user.userName.toLowerCase().indexOf(args.toLowerCase()) === 0);
  }

}
