import {
    trigger,
    state,
    style,
    animate,
    transition,
} from '@angular/animations';

export const fadeAnimation = trigger('fade', [
    transition(':enter', [
        style({ opacity: 0 }),
        animate('0.7s', style({ opacity: 1 })),
    ]),
    transition(':leave', [
        animate('0.5s', style({ opacity: 0 }))
    ])
]);
