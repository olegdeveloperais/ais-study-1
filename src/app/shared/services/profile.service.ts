import { Injectable } from '@angular/core';
import { IUser } from '../models/interfaces/app.models';
import { usersConst } from '../models/const/app.const';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private subject = new Subject<any>();
  // usersData = usersConst;

  constructor() {
    // this.setCurrentUser(this.usersData[0]);
  }

  setCurrentUser(user: IUser) {
    this.subject.next(user);
  }

  getCurrentUser(): Observable<any> {
    return this.subject.asObservable();
  }

}
