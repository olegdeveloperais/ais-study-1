import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { usersConst } from '../models/const/app.const';
import { IUser } from '../models/interfaces/app.models';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private subject = new BehaviorSubject<any>(usersConst);
  constructor() { }

  getUsers(): Observable<any> {
    return this.subject.asObservable();
  }

  getUserForEdit(userIndex) {
    return this.subject.value[userIndex];
  }

  addUser(profileForm): void {
    const userData = profileForm.value;
    this.subject.next([...this.subject.value, userData]);
  }
  editUser(userId, profileForm): void {
    const userData = profileForm.value;
    const usersData = [...this.subject.value];
    usersData[userId] = userData;

    console.log(usersData);
    this.subject.next(usersData);
  }
  deleteUser(userIndex) {
    this.subject.value.splice(userIndex, 1);
    // const subjectUpdatedValue = [...this.subject.value];
    // subjectUpdatedValue.splice(userIndex, 1);
    // this.subject.next(subjectUpdatedValue);
  }

}

