import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { lengthValidator } from '../shared/validators/lengthValidator.validator';
import { UsersService } from '../shared/services/users.service';
import { fadeAnimation } from '../shared/animations/fade.animation';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../shared/services/profile.service';

import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'app-react-form',
    animations: [fadeAnimation],
    templateUrl: './react-form.component.html',
    styleUrls: ['./react-form.component.scss']
})
export class ReactFormComponent {
    editorMode = false;
    userId;
    profileForm = this.fb.group({
        userName: ['', [Validators.required, lengthValidator(3, 9)]],
        userAge: ['', Validators.required],
        userOccupation: ['', [Validators.required, lengthValidator(4, 15)]],
        userImage: ['']
        // address: this.fb.group({
        //     street: [''],
        //     city: [''],
        // }),
    });

    matcher = new MyErrorStateMatcher();

    constructor(private fb: FormBuilder,
        private usersService: UsersService,
        private profileService: ProfileService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
        if (this.activatedRoute.params['value'].id) {
            this.userId = this.activatedRoute.params['value'].id;
            this.editorMode = true;
            this.getUser(this.userId);
        }

    }
    getUser(userIndex) {
        const userData = this.usersService.getUserForEdit(userIndex);
        this.profileForm.setValue({ ...userData });
    }

    onSubmit() {
        this.imageUrlCheck();
        if (this.editorMode) {
            this.editUser();
        } else {
            this.addUser();
        }
        this.profileService.setCurrentUser(this.profileForm.value);
        this.profileForm.reset();

        console.log(this.profileForm.value);
        this.router.navigate(['/main']);
    }

    imageUrlCheck() {
        if (!this.profileForm.controls['userImage'].value) {
            this.profileForm.controls['userImage'].setValue('https://randomuser.me/api/portraits/lego/1.jpg');
        }
    }

    addUser() {
        this.usersService.addUser(this.profileForm);
    }

    editUser() {
        this.usersService.editUser(this.userId, this.profileForm);
    }
}

